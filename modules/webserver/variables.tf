variable vpc_id{}
variable ip_address{}
variable env_prefix{}
variable image_name{}
variable public_key_location{}
variable instance_type{}
variable subnet_id{}
variable avail_zone{}
