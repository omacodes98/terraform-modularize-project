# Modularize Project 

## Table of Contents

- [Project Description](#project-description)

- [Technologies Used](#technologies-used)

- [Steps](#steps)

- [Installation](#installation)

- [Usage](#usage)

- [How to Contribute](#how-to-contribute)

- [Questions](#questions)

- [References](#references)

- [License](#license)

## Project Description

Divide Terraform resources into reusable modules 

## Technologies Used 

* Terraform

* AWS 

* Docker 

* Linux 

* Git 

## Steps 

Step 1: Create files for main components of project like output, variables and providers 

    touch output.tf variables.tf providers.tf 

[Files for Main Components](/images/01_create_files_for_components_in_the_main_file.png)

Step 2: Move all the output configuration in main.tf to the output file 

[Moving output configuration to output file](/images/02_move_all_the_output_configurations_to_the_output_file.png)

Step 3: Move all  the variables from main file to variable files 

[Move variables to variables files](/images/03_move_variables_from_main_file_to_variables_file.png)

Step 4: Create a directory called modules 

    mkdir modules 

[Created Modules directory](/images/04_create_a_directory_called_modules.png)

Step 5: Create a folders for the modules you want to create insides the modules directory

    mkdir modules/subnets modules/webserver

[Modules folders created](/images/05_create_folders_for_the_modules_inside_the_modules_directory.png)

Step 6: Create main, output and variables file for each module 

    touch subnet/main.tf subnet/variables.tf subnet/output.tf webserver/main.tf webserver/variables.tf webserver/output.tf 

[Created necessary files for each module](/images/06_create_main_output_and_variables_file_for_each_module.png)

Step 7: Move the subnet configurations from the main file in the root to the main file in subnet directory 

[Moved Subnet configuration](/images/07_move_the_subnet_configurations_from_the_main_root_file_to_subnet_module_main_file.png)


Step 8: Make subent main file completely configurable by making it dynamic and removing any hard coded values that will be needed to be changed in the future 

[Making Subnet main file completely configurable](/images/08_in_the_subnet_main_file_make_it_completely_configurable.png)

Step 9: Put needed variables in subnet variables files

[variables in subnet variables files](/images/09_put_needed_variables_in_subnet_variables_files.png)

Step 10: Declare module in main root file 

[Declare subent module](/images/10_declare_module_in_main_root_file.png)

Step 11: Check for any code breaks that could occur from modularizing, in this case there will be a code break in the instance resource for ec2 this is because the subnet is not in the main root file. Furthermore we therefore need to id, to do this we will need to go to subnet output file and an insert an output component that will display the subnet id

[Code break Component](/images/11_modularizing_will_cause_code_to_break_because_we_dont_have_the_subnet_in_the_main_root_file_and_we_need_the_id_to_do_this_we_will_need_to_go_to_subnet_output_file_and_an_output_component_that_will_display_the_subnet_id.png)

[Subnet output](/iamges/12_subnet_output.png)

Step 12: Reference the output for the id in main root file 

[Referencing Subnet id](/images/13_reference_it_in_main_root_file_i_am_referencing_one_of_the_resources_of_the_module_and_then_i_need_the_name_of_the_module_and_reference_the_output_name_in_subnet_output.png)

Step 13: Go to the terminal and perform terraform init in the terraform directory. This is because when you change an existing module or add a new module we have to run terraform init

    terraform init 

[Subnet Terraform init](/images/14_do_terraform_init_this_is_because_when_you_change_an_existinga_module_or_add_a_new_module_we_have_to_run_terraform_init.png)

Step 14: Apply changes 

[Apply 1](/images/15_apply.png)

[Apply 2](/images/15_apply1.png)

[Apply 3](/images/15_apply2.png)

[Apply 4](/images/15_apply3.png)

[Apply 5](/images/15_apply4.png)

[Apply 6](/images/15_apply5.png)

[Apply 7](/images/15_apply6.png)

Step 15: Move all needed components for webserver from root main to module webserver main 

[Webserver Components moved](/images/16_move_all_needed_components_for_webserver_from_root_main_to_module_webeserver_main_.png)

Step 16: Parametalize all values needed in webserver main module file to make it dynamic 

[Parametalizing webserver main](/images/17_parametalize_all_values_needed_in_webserver_main_module_file.png)

Step 17: Declare all the variables in webserver variables module file 

[Variables file](/images/19_declare_all_the_variables_in_webserver_variables_module_file.png)

Step 18: Declare module in main root file 

[Declare webserver module](/images/20_reference_values_in_main_file.png)

Step 19: Use terraform init to install modules 

    terraform init 

[Webserver terraform init](/images/21_use_terraform_init_to_install_module.png)

Step 20: Apply changes 

[Apply 8](/images/22_terraform_apply.png)

[Apply 9](/images/23_terraform_apply1.png)

## Installation 

    brew install terraform 

## Usage 

    Terraform apply 

## How to Contribute

1. Clone the repo using $ git clone git@gitlab.com:omacodes98/terraform-modularize-project.git

2. Create a new branch $ git checkout -b your name 

3. Make Changes and test 

4. Submit a pull request with description for review



## Questions

Feel free to contact me for further questions via: 

Gitlab: https://gitlab.com/omacodes98

Email: omacodes98@gmail.com

## References

https://gitlab.com/omacodes98/terraform-modularize-project

## License

The MIT License 

For more informaation you can click the link below:

https://opensource.org/licenses/MIT

Copyright (c) 2022 Omatsola Beji.